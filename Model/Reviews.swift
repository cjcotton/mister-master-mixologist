//
//  Reviews.swift
//  MisterMasterMixologist
//
//  Created by cjcotton on 10/13/18.
//  Copyright © 2018 cjcotton. All rights reserved.
//

import Foundation
import UIKit

class Reviews {
    
    static var globalReviews:[Review] = [];
    
    init()
    {
        //possible firebase extension
    }
    
    func addReview(aReview:Review){
        Reviews.globalReviews.append(aReview)
    }
    
    
    func retrieveReview(index:Int) -> Review {
        return Reviews.globalReviews[index]
    }
    
}

class Review {
    var cocktailName = ""
    var restaurantName = ""
    var cocktailImage:UIImage;
    var rating:Double;
    var review:String
    
    init(aName:String, aRestaurant:String, aImage:UIImage, aRating:Double, aReview:String)
    {
        cocktailName = aName
        restaurantName = aRestaurant
        cocktailImage = aImage
        rating = aRating
        review = aReview
    }
    
    func changePicture(pic:UIImage){
        cocktailImage = pic
    }
    
}
