//
//  FirebaseImageStorage.swift
//  MisterMasterMixologist
//
//  Created by cjcotton on 11/13/18.
//  Copyright © 2018 cjcotton. All rights reserved.
//

import Foundation
import Firebase

class FirebaseImageStorage
{
    
    var photoStore = Storage.storage(url: "gs://mistermastermixologist.appspot.com")
    
    var photoStoreReference:StorageReference?
    var photoListReference:StorageReference?
    
    init()
    {
        photoStoreReference = photoStore.reference()
        photoListReference = photoStoreReference?.child("drinkPhotos/")
        
    }
    
    func storePhotoWithKey(key: String, imageData:NSData, completion: @escaping ()-> Void)
    {
        let refOfPhoto = photoListReference?.child(key)
        
        let photoSend = imageData
        
        refOfPhoto?.putData(photoSend as Data, metadata: nil){ (metadata, error) in
            guard let metadata = metadata else {
                print("Error uploading photo")
                return
                
            }
            
            refOfPhoto?.downloadURL{ (url, error) in
                guard let downloadURL = url else {
                    print("Error retrieving download URL")
                    return
                }
                print(downloadURL)
                
            }
            
            completion()
            
            
            
            
        
        }
        
        
    }
    
    /*func getPhoto(key: String) -> UIImage
    {
        let refOfPhoto = photoListReference?.child(key)
        var actualPhoto:UIImage
        var photoExists = false
        
        refOfPhoto?.getData(maxSize: 100 * 1024 * 1024){ data, error in
            if let error = error
            {
                print("Can't retrieve image")
            }
            else
            {
                actualPhoto = UIImage(data: data!)!
                photoExists = true
            }
            
        }
        
        return actualPhoto
    }*/
    
    
    func removePhoto(key: String)
    {
        let refOfPhoto = photoListReference
        
        refOfPhoto?.delete {  error in
            if let error = error {
                print("Photo could not delete")
                print(error)
            }
            else
            {
                //nothing in here, deletion is successful
            }
            
        }
    }
    
}
