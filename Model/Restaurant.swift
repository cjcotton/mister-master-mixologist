//
//  Restaurant.swift
//  MisterMasterMixologist
//
//  Created by cjcotton on 11/17/18.
//  Copyright © 2018 cjcotton. All rights reserved.
//

import Foundation
import MapKit

class Restaurant {
    
    let restaurantName:String
    let restCoordinate: CLLocationCoordinate2D
    
    init(aRestauarantName: String, aRestCoordinate: CLLocationCoordinate2D)
    {
        self.restaurantName = aRestauarantName
        self.restCoordinate = aRestCoordinate
    }
    
    
}
