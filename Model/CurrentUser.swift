//
//  CurrentUser.swift
//  MisterMasterMixologist
//
//  Created by cjcotton on 10/13/18.
//  Copyright © 2018 cjcotton. All rights reserved.
//

import Foundation

class CurrentUser {
    
    var currentUsername = ""
    
    init(aUser: String){
        currentUsername = aUser
    }
    
}
