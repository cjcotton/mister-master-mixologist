//
//  ViewController.swift
//  MisterMasterMixologist
//
//  Created by cjcotton on 10/3/18.
//  Copyright © 2018 cjcotton. All rights reserved.
//

import UIKit
import Firebase


class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var recentReviews:Reviews = Reviews();
    
    let operatingUser = CurrentUser(aUser: "");
    
    let fireIS = FirebaseImageStorage()
    
    @IBOutlet weak var reviewTableView: UITableView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        print(operatingUser.currentUsername)
        var runObserver = true
        //let drinkReference = Database.database().reference().child("DrinkList")
        let drinkReference = Database.database().reference().child("DrinkList")
        print(drinkReference)
        print("At main menu")
        
        //handler to cease observing
        var counter = 0;
        var postCounter = 0;
        var preCounter = 0;
        var cldStrInts:[Int] = []
        
        var listHasNoImages = true
        
        var hnd:UInt = 0
        if runObserver == true {
            hnd = drinkReference.observe(.value, with: { snapshot in
                for item in snapshot.children {
                    //print(drinkReference)
                    
                    print("Something is happening in here")
                    let drinkFIR = item as! DataSnapshot
                    //print(drinkFIR)
                    let dictFIR = drinkFIR.value as! NSDictionary
                    
                    var imageIn:UIImage = UIImage(named: "Default")!;
                    
                    let drinkIN = dictFIR["drinkName"] as! String
                    //print(drinkIN)
                    //print(dictFIR)
                    let restaurantIN = dictFIR["drinkRestaurant"] as! String
                    //print(restaurantIN)
                    let ratingIN = dictFIR["drinkRating"] as! Double
                    //print(ratingIN)
                    let reviewIN = dictFIR["drinkReview"] as! String
                    
                    let reviewHasImage = dictFIR["hasImageInStorage"] as! String
                    
                    let newRev:Review = Review(aName: drinkIN, aRestaurant:restaurantIN, aImage:imageIn, aRating:ratingIN, aReview:reviewIN)
                    
                    
                    if(counter >= Reviews.globalReviews.count){
                        print(hnd)
                        print(Reviews.globalReviews.count)
                        print(Reviews.globalReviews)
                        print("Grabbing " +  drinkIN)
                        self.recentReviews.addReview(aReview: newRev)
                    }
                    
                    let refOfPhoto = self.fireIS.photoListReference?.child(String(counter))
                    
                    print(reviewHasImage)
                    
                    if reviewHasImage == String(true)
                    {
                        listHasNoImages = false
                        cldStrInts.append(counter)
                        preCounter = preCounter + 1
                        refOfPhoto?.getData(maxSize: 100 * 1024 * 1024){ data, error in
                            if let error = error {
                                print("There is an Error")
                                //print(error)
                                imageIn = UIImage(named: "Default")!
                            }
                            else{
                                imageIn = UIImage(data: data!)!
                            }
                            
                            print(counter)
                            //error here
                            Reviews.globalReviews[cldStrInts[postCounter]].changePicture(pic: imageIn)
                            postCounter = postCounter + 1
                            self.reviewTableView.reloadData()
                            print("preCounter")
                            if(postCounter >= preCounter)
                            {
                                print("removing observer")
                                drinkReference.removeObserver(withHandle: hnd)
                            }
                            
                        }
                    }
                    
                    
                    self.reviewTableView.reloadData()
                    counter = counter + 1
                    
                }
                
                if(counter == 0 || listHasNoImages == true)
                {
                    print("counter 0")
                    drinkReference.removeObserver(withHandle: hnd)
                }
                
                self.reviewTableView.reloadData()
                //removing observer
                //print("removing observer")
                //drinkReference.removeObserver(withHandle: hnd)
                
                
                
            })
        }
        print("At main view")
        runObserver = false
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Reviews.globalReviews.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let decVar = Reviews.globalReviews.count-1
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellForReviews", for: indexPath) as! ReviewTableViewCell
        
        let valuesForReviews = Reviews.globalReviews
        
        cell.drinkCell.text = valuesForReviews[decVar-indexPath.row].cocktailName
        cell.restaurantLabel.text = valuesForReviews[decVar-indexPath.row].restaurantName
        cell.ratingCell.text = String(valuesForReviews[decVar-indexPath.row].rating)
        cell.reviewCell.text = valuesForReviews[decVar-indexPath.row].review
        cell.drinkImageCell.image = valuesForReviews[decVar-indexPath.row].cocktailImage
        
        
        return cell
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let decVar = Reviews.globalReviews.count-1
        
        //edit1 goes to review page
        if(segue.identifier == "toEdit1"){
            let indexSelected: IndexPath = self.reviewTableView.indexPath(for: sender as! UITableViewCell)!
        
            //might need to undergo changes in future
            let review:Review = recentReviews.retrieveReview(index: decVar-indexSelected.row)
        
        
                if let otherVC:FullReviewController = segue.destination as? FullReviewController {
                    otherVC.selectedReview = review
                    /*otherVC.cocktailTitle.text! = review.cocktailName
                    otherVC.imageOfCocktail.image! = review.cocktailImage
                    otherVC.ratingOfCocktail.text! = String(review.rating) + " star(s)"
                    otherVC.reviewOfCocktail.text! = review.review*/
            
            }
        }
    }
    
    @IBAction func backToMain(segue: UIStoryboardSegue){
        //nothing in here
        print("back button pressed")
    }
    
    @IBAction func backFromMap(segue: UIStoryboardSegue){
        //nothing in htere
        print("back from map")
    }
    
    @IBAction func backFromRestaurantScope(segue: UIStoryboardSegue){
        //nothing in htere
        print("back from restraunt table view")
    }
    
    @IBAction func backFromSettings(segue: UIStoryboardSegue){
        //nothing in htere
        print("back from settings")
    }
    
  
    
    


}

