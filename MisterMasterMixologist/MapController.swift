//
//  MapController.swift
//  MisterMasterMixologist
//
//  Created by cjcotton on 11/15/18.
//  Copyright © 2018 cjcotton. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapController: UIViewController, UITextFieldDelegate, MKMapViewDelegate, CLLocationManagerDelegate {
    
    
    @IBOutlet weak var searchBar: UITextField!
    @IBOutlet weak var segControl: UISegmentedControl!
    @IBOutlet weak var barMapView: MKMapView!
    @IBOutlet weak var mapZoom: UIStepper!
    
    var locationManager: CLLocationManager!
    var selectedRestaurantCoordinate:CLLocationCoordinate2D!
    var goToUserLocation:Bool = true
    var restaurantSelected:String = ""
    
    override func viewDidLoad(){
        super.viewDidLoad()
        self.barMapView.delegate = self
        self.barMapView.showsUserLocation = true
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.searchBar.delegate = self
        self.mapZoom.wraps = false
        self.mapZoom.autorepeat = true
        self.mapZoom.maximumValue = 12
        self.mapZoom.minimumValue = 0
        self.mapZoom.value = 3
        
        
        let startingZoom = 59.00 * pow(0.50, self.mapZoom.value)
        
        let mapSpan = MKCoordinateSpanMake(startingZoom, startingZoom)
        //possible if else
        
        let mapRegion = MKCoordinateRegion(center: self.barMapView.region.center, span: mapSpan)
        print(self.barMapView.region.center)
        self.barMapView.setRegion(mapRegion, animated: true)
        
    
        self.checkAuthentication()
        
        
        //first seeting
        print(barMapView.region.span.latitudeDelta)
        
    }
    
    override func didReceiveMemoryWarning(){
        super.didReceiveMemoryWarning()
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        //make sure to unwrap later
        selectedRestaurantCoordinate = view.annotation!.coordinate
        self.restaurantSelected = view.annotation!.title!!
    }
    
    func checkAuthentication()
    {
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            // Request when-in-use authorization initially
            locationManager.requestWhenInUseAuthorization()
            break
            
        case .restricted, .denied:
            // Disable location features
            self.barMapView.showsUserLocation = false
            print("Disabled")
            break
            
        case .authorizedWhenInUse, .authorizedAlways:
            // Enable location features
            print("Enabled")
            locationManager.startUpdatingLocation()
            break
        }
    }
    
    @IBAction func mapSwitch(_ sender: Any) {
        
        switch(segControl.selectedSegmentIndex)
        {
        case 0:
            barMapView.mapType = MKMapType.standard
        case 1:
            barMapView.mapType = MKMapType.satellite
        case 2:
            barMapView.mapType = MKMapType.hybrid
        default:
            barMapView.mapType = MKMapType.standard
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if goToUserLocation == true {
            guard let userCoord: CLLocationCoordinate2D = locationManager.location?.coordinate else{return}
            let userMapRegion = MKCoordinateRegion(center: userCoord, span: barMapView.region.span)
            self.barMapView.setRegion(userMapRegion, animated: true)
            goToUserLocation = false
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse || status == .authorizedAlways {
            locationManager.startUpdatingLocation()
        }
    }
    
    
    @IBAction func zoomAction(_ sender: UIStepper) {
        //mapview.region.span longitude latitude
        let latDelta = 59.00 * pow(0.50, Double(sender.value))
        let longDelta = 59.00 * pow(0.50, Double(sender.value))
        var region = self.barMapView.region
        region.span.latitudeDelta = latDelta
        region.span.longitudeDelta = longDelta
        barMapView.setRegion(region, animated: true)
    }
    
    
    @IBAction func searchButton(_ sender: Any) {
        barMapView.removeAnnotations(barMapView.annotations)
        print(self.barMapView.region.center)
        let req = MKLocalSearchRequest()
        req.naturalLanguageQuery = self.searchBar.text! //+ " restaurant"
        req.region = barMapView.region
        let search = MKLocalSearch(request: req)
        
        search.start { response, _ in
            guard let response = response else{
                return
            }
            
            var restMapItems:[MKMapItem] = []
            restMapItems = response.mapItems
            
            if restMapItems.count > 0 {
                for i in 0...restMapItems.count-1 {
                    let loc = restMapItems[i].placemark
                    let mark = MKPointAnnotation()
                    mark.title = loc.name!
                    mark.subtitle = loc.locality!
                    mark.coordinate = loc.location!.coordinate
                    
                    self.barMapView.addAnnotation(mark)
                }
            }
        }
        
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        //print(segue.identifier)
        
        if(segue.identifier == "mapToRestaurant"){
            let restObject = Restaurant(aRestauarantName: restaurantSelected, aRestCoordinate: selectedRestaurantCoordinate)
            if let restView:RestaurantDetailView = segue.destination as? RestaurantDetailView {
                restView.userChoice = restObject
            }
        }
        
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        
        print("firing")
        
        if(identifier == "mapToRestaurant")
        {
            if self.restaurantSelected == ""{
                return false
            }
            
            else{
                return true
            }
        }
        
        else{
            return true
        }
    }
    
    
    @IBAction func backFromRestView(segue: UIStoryboardSegue){
        //nothing in htere
        print("back from restView")
    }
    
    @IBAction func currentBackPushed(_ sender: Any) {
        locationManager.stopUpdatingLocation()
        print("stopped updating")
    }
    
}
