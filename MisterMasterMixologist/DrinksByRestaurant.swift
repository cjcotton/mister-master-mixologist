//
//  DrinksByRestaurant.swift
//  MisterMasterMixologist
//
//  Created by cjcotton on 11/18/18.
//  Copyright © 2018 cjcotton. All rights reserved.
//

import UIKit

class DrinksByRestaurant: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    @IBOutlet weak var restaurantTableView: UITableView!
    
    var listOfRestaurants:[String] = []
    
    
    override func viewDidLoad(){
        super.viewDidLoad()
        self.restaurantTableView.delegate = self
        self.restaurantTableView.dataSource = self
        
        let reviewsObj:Reviews = Reviews()
        
        //loading section
        if(Reviews.globalReviews.count > 0){
            for i in 0...Reviews.globalReviews.count-1{
                
                var restaurantExists = false
                
                if self.listOfRestaurants.count > 0 {
                    for j in 0...self.listOfRestaurants.count-1 {
                        
                        if reviewsObj.retrieveReview(index: i).restaurantName.lowercased() == listOfRestaurants[j].lowercased(){
                            restaurantExists = true
                        }
                        
                    }
                
                    if restaurantExists == false {
                        listOfRestaurants.append(reviewsObj.retrieveReview(index: i).restaurantName)
                    }
                    
                }
                
                else{
                    listOfRestaurants.append(reviewsObj.retrieveReview(index: i).restaurantName)
                }
                
            }
        }
        
        self.restaurantTableView.reloadData()
    }
    
    override func didReceiveMemoryWarning(){
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection: Int) -> Int{
        print("table view is counted")
        return self.listOfRestaurants.count
    }
    

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "restaurantCell", for: indexPath) as! BasicRestaurantCell
        cell.restaurantCellLabel.text = listOfRestaurants[indexPath.row]
        return cell
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        
        if(segue.identifier == "tableViewToTV")
        {
            let indexSelected: IndexPath = self.restaurantTableView.indexPath(for: sender as! UITableViewCell)!
            
            let restaurantNme = self.listOfRestaurants[indexSelected.row]
            
            if let otherVC:RestaurantDrinkReviews = segue.destination as? RestaurantDrinkReviews{
                otherVC.stringChosen = restaurantNme
            }
            
        }
        
    }
    
    @IBAction func backFromRestList(segue: UIStoryboardSegue){
        //nothing in htere
        print("back from restraunt table view")
    }
    
    
    
    
}
