//
//  SettingsController.swift
//  MisterMasterMixologist
//
//  Created by cjcotton on 11/19/18.
//  Copyright © 2018 cjcotton. All rights reserved.
//

import UIKit
import CoreLocation
import Photos

class SettingsController: UIViewController {
    
    
    @IBOutlet weak var curLocStatus: UILabel!
    var locManager: CLLocationManager!
    @IBOutlet weak var photoLibStatus: UILabel!
    
    override func viewDidLoad(){
        super.viewDidLoad()
        self.locManager = CLLocationManager()
        self.checkLocAuthentication()
        self.checkPhotoAuthentication()
        
    }
    
    override func didReceiveMemoryWarning(){
        super.didReceiveMemoryWarning()
    }
    
    func checkLocAuthentication()
    {
        
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            self.curLocStatus.text = "OFF"
            break
            
        case .restricted, .denied:
            self.curLocStatus.text = "OFF"
            break
            
        case .authorizedWhenInUse, .authorizedAlways:
            self.curLocStatus.text = "ON"
            break
        }

    }
    
    func checkPhotoAuthentication(){
        let authorizationStatus = PHPhotoLibrary.authorizationStatus()
        switch authorizationStatus {
        case .authorized:
            self.photoLibStatus.text = "ON"
            break
        case .restricted,.denied, .notDetermined:
            self.photoLibStatus.text = "OFF"
        }
    }
    
    
    
}
