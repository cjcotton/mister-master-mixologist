//
//  RestaurantDetailView.swift
//  MisterMasterMixologist
//
//  Created by cjcotton on 11/17/18.
//  Copyright © 2018 cjcotton. All rights reserved.
//

import UIKit

class RestaurantDetailView: UIViewController {
    
    var userChoice:Restaurant!
    var yelpData:YelpApiDataSource = YelpApiDataSource()
    
    @IBOutlet weak var restaurantLabel: UILabel!
    @IBOutlet weak var latitudeLabel: UILabel!
    @IBOutlet weak var longitudeLabel: UILabel!
    @IBOutlet weak var strAddLabel: UILabel!
    @IBOutlet weak var cityAddLabel: UILabel!
    @IBOutlet weak var phoneNumLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    
    
    
    override func viewDidLoad(){
        super.viewDidLoad()
        self.restaurantLabel.text = userChoice.restaurantName
        let restLat = userChoice.restCoordinate.latitude
        let restLong = userChoice.restCoordinate.longitude
        self.latitudeLabel.text = String(restLat)
        self.longitudeLabel.text = String(restLong)
        let restTerm = userChoice.restaurantName
        self.cityAddLabel.text = ""
        self.strAddLabel.text = ""
        self.phoneNumLabel.text = ""
        self.ratingLabel.text = ""
        self.priceLabel.text = ""
        
        
        yelpData.retrieveAPIData(term: restTerm, lat: restLat, long: restLong, completion: {(rs0, rs1, rs2, rs3, rs4) in
            DispatchQueue.main.async{
            self.strAddLabel.text = rs0
            self.cityAddLabel.text = rs1
            self.phoneNumLabel.text = rs2
            self.priceLabel.text = rs3
            self.ratingLabel.text = rs4
            }
            
        })
        
    }
    
    /*func retrieveFromYelp(term: String, lat: Double, long: Double){
     
     let urlStr = "https://api.yelp.com/v3/businesses/search?term=\"" + term +
     "\"&latitude=" + String(lat) + "&longitude=" + String(long) + "&limit=1"
     
     let urlEnc = urlStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
     print(urlEnc)
     let url = URL(string: urlEnc!)
     var urlWithHeader = URLRequest(url: url!)
     urlWithHeader.setValue("Bearer cIYHCgy2mxWIVDvo2ZeeYY54TNFa_VoVigxfEYk-ZUcpac2W2j35VlbgPjJmBy4E4XMKhDzGkyPD0PUJoBw5Tcu8MDYanK7V0APyMLGMXdcS3BSn1H2r3oxmlZvtW3Yx", forHTTPHeaderField: "Authorization")
     let urlSession = URLSession.shared
     let jsonQuery = urlSession.dataTask(with: urlWithHeader, completionHandler: { data, response, error -> Void in
     if error != nil {
     print(error!.localizedDescription)
     }
     print("Inside")
     
     var err: NSError?
     if data == nil {
     print("nil")
     }
     var jsonResult = (try! JSONSerialization.jsonObject(with: data!, options: .allowFragments)) as! NSDictionary
     
     print(jsonResult)
     
     let busArray = jsonResult["businesses"] as! NSArray
     print(busArray)
     let restInfo = busArray[0] as! NSDictionary
     
     let restPhoneNum = String((restInfo["display_phone"] as? NSString)!)
     let restPrice = String((restInfo["price"] as? NSString)!)
     let restRating = String((restInfo["rating"] as? NSNumber)!.doubleValue)
     
     let stepToAddress1 = restInfo["location"] as? NSDictionary
     //let restAddress = String((stepToAddress1!["dispay_address"] as? NSDictionary)!)
     let stepToAddress2 = stepToAddress1!["display_address"] as? NSArray
     let restStreetAdd = String((stepToAddress2![0] as? NSString)!)
     let restCityAdd = String((stepToAddress2![1] as? NSString)!)
     
     print(restStreetAdd)
     print(restPhoneNum)
     print(restPrice)
     print(restRating)
     print(restInfo)
     
     DispatchQueue.main.async{
     self.strAddLabel.text = restStreetAdd
     self.cityAddLabel.text = restCityAdd
     self.phoneNumLabel.text = restPhoneNum
     self.priceLabel.text = restPrice
     self.ratingLabel.text = restRating
     }
     
     })
     
     jsonQuery.resume()
     
     }*/
    
    override func didReceiveMemoryWarning(){
        super.didReceiveMemoryWarning()
    }
    

    
    
}
