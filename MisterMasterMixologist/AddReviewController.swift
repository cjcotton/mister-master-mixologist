//
//  AddReviewController.swift
//  MisterMasterMixologist
//
//  Created by cjcotton on 10/14/18.
//  Copyright © 2018 cjcotton. All rights reserved.
//

import UIKit
import Firebase
import Photos

class AddReviewController: UIViewController, UITextFieldDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @IBOutlet weak var drinkField: UITextField!
    @IBOutlet weak var restaurantField: UITextField!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var stepper: UIStepper!
    @IBOutlet weak var reviewTextBox: UITextView!
    
    @IBOutlet weak var drinkImage: UIImageView!
    
    let firebasePhotoStore = FirebaseImageStorage()
    var drinkImageData:NSData?
    var imPicker = UIImagePickerController()
    
    var shouldSubmit:Bool = false;
    var hasImage:Bool = false
    
    var rating = 4.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imPicker.delegate = self
        stepper.wraps = false
        stepper.autorepeat = true
        stepper.maximumValue = 10
        stepper.minimumValue = 2
        
        self.ratingLabel.text = String(Int(stepper.value)/2)
        rating = stepper.value/2
        
        self.drinkField.delegate = self
        self.restaurantField.delegate = self
        
        self.reviewTextBox.text = "Type a review here ..."
        self.reviewTextBox.backgroundColor = UIColor.lightGray
        
        self.drinkImage.image = UIImage(named: "Default")
        drinkImageData = UIImagePNGRepresentation(self.drinkImage.image!) as NSData?
        
        
    }
    
    override func didReceiveMemoryWarning(){
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func stepperAction(_ sender: UIStepper) {
        self.ratingLabel.text = (String(Double(sender.value)/2.0))
        rating = Double(self.ratingLabel!.text!)!
    }
    
    func checkPermission() {
        let authorizationStatus = PHPhotoLibrary.authorizationStatus()
        switch authorizationStatus {
            case .authorized:
                print("Access is granted")
                break
            case .notDetermined:
                PHPhotoLibrary.requestAuthorization({
                    (newStatus) in
                    print("status is currently \(newStatus)")
                    if newStatus == PHAuthorizationStatus.authorized{
                        print("GOAL")
                    }
                })
                break
            case .restricted:
                print("User is not allowed to access to photo album.")
                break
            
            case .denied:
                print("User is denied")
                PHPhotoLibrary.requestAuthorization({
                    (newStatus) in
                    print("status is currently \(newStatus)")
                    if newStatus == PHAuthorizationStatus.authorized{
                        print("GOAL")
                    }
                })
            
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let imageSelected = info[UIImagePickerControllerOriginalImage] as? UIImage
        drinkImageData = UIImagePNGRepresentation(imageSelected!) as NSData?;
        print("changed")
        self.drinkImage.image = imageSelected
        self.hasImage = true
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func changeTheImage(_ sender: Any) {
        checkPermission()
        
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            self.imPicker.allowsEditing = false
            self.imPicker.sourceType = UIImagePickerControllerSourceType.camera
            self.imPicker.cameraCaptureMode = .photo
            self.imPicker.modalPresentationStyle = .fullScreen
            self.present(self.imPicker, animated:true, completion:nil)
        }
            
        else {
            self.imPicker.allowsEditing = false
            self.imPicker.sourceType = .photoLibrary
            self.imPicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
            self.imPicker.modalPresentationStyle = .popover
            self.present(self.imPicker, animated: true, completion:nil)
        }
    }
    
    
    
    @IBAction func submitAction(_ sender: Any) {
        if !(self.drinkField.text?.isEmpty)! && !(self.restaurantField.text?.isEmpty)! {
            let cocktail = self.drinkField.text
            let restaurant = self.restaurantField.text
            //let image = self.drinkImage.image
            let theRating = rating
            let review = self.reviewTextBox.text
            
            firebasePhotoStore.storePhotoWithKey(key: String(Reviews.globalReviews.count), imageData: drinkImageData!, completion: {
                print("here at the end")
                print(Reviews.globalReviews)
                self.shouldSubmit = true;
                self.performSegue(withIdentifier: "submitToMain", sender: self)
            })
            
            //let newReview:Review = Review(aName: cocktail!, aRestaurant: restaurant!, aImage: image!, aRating:theRating, aReview:review!)
            
            //let reviewClass = Reviews()
            
            //reviewClass.addReview(aReview: newReview)
            
            //Database.database().reference().child("DrinkList").child(String(Reviews.globalReviews.count)).setValue("Hi Everyone")
            
            let coreRoot = Database.database().reference().child("DrinkList/").child(String(Reviews.globalReviews.count))
            
            coreRoot.child("drinkName").setValue(cocktail, withCompletionBlock: { (error, snapshot) in
                if error != nil {
                    print("oops, an error")
                } else {
                    print("completed")
                }
            })
            //print("set value 1")
            coreRoot.child("drinkRestaurant").setValue(restaurant, withCompletionBlock: { (error, snapshot) in
                if error != nil {
                    print("oops, an error")
                } else {
                    print("completed")
                }
            })
            //print("set value 2")
            coreRoot.child("drinkRating").setValue(theRating, withCompletionBlock: { (error, snapshot) in
                if error != nil {
                    print("oops, an error")
                } else {
                    print("completed")
                }
            })
            //print("set value 3")
            coreRoot.child("drinkReview").setValue(review, withCompletionBlock: { (error, snapshot) in
                if error != nil {
                    print("oops, an error")
                } else {
                    print("completed")
                }
                
                
            })
            
            coreRoot.child("hasImageInStorage").setValue(String(hasImage), withCompletionBlock: { (error, snapshot) in
                if error != nil {
                    print("oops, an error")
                }
                else {
                    print("completed")
                }
            })
            
            print("is done")
            
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("Overridin")
        //nothing in here
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "submitToMain"
        {
            print("Core is complete")
            return shouldSubmit
        }
        else
        {
            print("back shouldPerformSegue")
            return true
        }
    }
    
    
}
