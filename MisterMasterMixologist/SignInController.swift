//
//  SignInController.swift
//  MisterMasterMixologist
//
//  Created by cjcotton on 10/13/18.
//  Copyright © 2018 cjcotton. All rights reserved.
//

import UIKit

class SignInController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    var uName = ""
    var passW = ""
    
    var shouldAuthenticate = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.usernameField!.delegate = self
        self.passwordField!.delegate = self
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destin = segue.destination as! ViewController
        
        if(segue.identifier == "toMain")
        {
            destin.operatingUser.currentUsername = uName
            passW = "###################"
        }
    }
    
    
    @IBAction func signInButton(_ sender: Any) {
        uName = self.usernameField!.text!;
        passW = self.passwordField!.text!;
        //should authenticate before performing segue
        shouldAuthenticate = true
        performSegue(withIdentifier: "toMain", sender: self)
        shouldAuthenticate = false
        
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        return shouldAuthenticate
    }
    
    
}
