//
//  FullReviewController.swift
//  MisterMasterMixologist
//
//  Created by cjcotton on 10/22/18.
//  Copyright © 2018 cjcotton. All rights reserved.
//

import UIKit

class FullReviewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    var selectedReview:Review?
    
    @IBOutlet weak var cocktailTitle: UILabel!
    @IBOutlet weak var imageOfCocktail: UIImageView!
    @IBOutlet weak var locationOfCocktail: UILabel!
    @IBOutlet weak var ratingOfCocktail: UILabel!
    @IBOutlet weak var reviewOfCocktail: UILabel!
    
    override func viewDidLoad(){
        super.viewDidLoad()
        self.cocktailTitle.text = selectedReview?.cocktailName
        self.imageOfCocktail.image = selectedReview?.cocktailImage
        self.ratingOfCocktail.text = String(selectedReview!.rating)
        self.reviewOfCocktail.text = selectedReview?.review
        self.locationOfCocktail.text = selectedReview?.restaurantName
        
        self.reviewOfCocktail.numberOfLines = 0
        self.reviewOfCocktail.lineBreakMode = .byTruncatingTail
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        //code goes here
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        dismiss(animated: true, completion: nil)
    }
    
    
}
