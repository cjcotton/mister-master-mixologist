//
//  RestaurantDrinkReviews.swift
//  MisterMasterMixologist
//
//  Created by cjcotton on 11/18/18.
//  Copyright © 2018 cjcotton. All rights reserved.
//

import UIKit

class RestaurantDrinkReviews: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var reviewTableView: UITableView!
    
    var restaurantReviews:[Review] = []
    var stringChosen:String = ""
    
    var reviewsObj:Reviews = Reviews()
    
    override func viewDidLoad(){
        super.viewDidLoad()
        self.reviewTableView.delegate = self
        self.reviewTableView.dataSource = self
        
        for i in 0...Reviews.globalReviews.count-1 {
            if(reviewsObj.retrieveReview(index: i).restaurantName.lowercased() == stringChosen.lowercased()){
                self.restaurantReviews.append(reviewsObj.retrieveReview(index: i))
            }
        }
        
        self.reviewTableView.reloadData()
    }
    
    override func didReceiveMemoryWarning(){
        super.didReceiveMemoryWarning()
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection: Int) -> Int{
        return self.restaurantReviews.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellForReviews", for: indexPath) as! ReviewTableViewCell
        
        cell.drinkCell.text = self.restaurantReviews[indexPath.row].cocktailName
        cell.restaurantLabel.text = self.restaurantReviews[indexPath.row].restaurantName
        cell.ratingCell.text = String(self.restaurantReviews[indexPath.row].rating)
        cell.reviewCell.text = self.restaurantReviews[indexPath.row].review
        cell.drinkImageCell.image = self.restaurantReviews[indexPath.row].cocktailImage
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        
        if(segue.identifier == "restRestTVToRev"){
            
            let indexSelected: IndexPath = self.reviewTableView.indexPath(for: sender as! UITableViewCell)!
            
            let review:Review = self.restaurantReviews[indexSelected.row]
            
            if let otherVC:FullReviewController2 = segue.destination as? FullReviewController2 {
                otherVC.selectedReview = review
            }
            
            
        }
        
    }
    
    @IBAction func backToRestReviewList(segue: UIStoryboardSegue){
        print("Back to RestaurantDrinkReviews")
    }
    
    
    
    
}
