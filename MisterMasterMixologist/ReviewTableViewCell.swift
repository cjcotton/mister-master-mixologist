//
//  ReviewTableViewCell.swift
//  MisterMasterMixologist
//
//  Created by cjcotton on 10/13/18.
//  Copyright © 2018 cjcotton. All rights reserved.
//

import UIKit

class ReviewTableViewCell: UITableViewCell {
    
    @IBOutlet weak var drinkCell: UILabel!
    @IBOutlet weak var restaurantLabel: UILabel!
    @IBOutlet weak var ratingCell: UILabel!
    @IBOutlet weak var reviewCell: UILabel!
    @IBOutlet weak var drinkImageCell: UIImageView!{
        didSet{
            drinkImageCell.layer.cornerRadius = drinkImageCell.bounds.width/2
        }
    }
    
    
    override func awakeFromNib(){
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool){
        super.setSelected(selected, animated: animated)
    }
    

    
}
