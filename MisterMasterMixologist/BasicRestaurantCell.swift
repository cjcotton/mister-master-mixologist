//
//  BasicRestaurantCell.swift
//  MisterMasterMixologist
//
//  Created by cjcotton on 11/18/18.
//  Copyright © 2018 cjcotton. All rights reserved.
//

import UIKit

class BasicRestaurantCell: UITableViewCell {
    
    
    @IBOutlet weak var restaurantCellLabel: UILabel!
    
    
    override func awakeFromNib(){
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool){
        super.setSelected(selected, animated: animated)
    }
    
}
